DROP DATABASE IF EXISTS mi_tiendita;
CREATE DATABASE mi_tiendita ;
use mi_tiendita;

DROP TABLE IF EXISTS `category`;
CREATE TABLE `category`  (
  `id` smallint NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` smallint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uq_no_repeating_category`(`name`)
);

DROP TABLE IF EXISTS `order`;
CREATE TABLE `order`  (
  `id` smallint NOT NULL AUTO_INCREMENT,
  `provider_id` smallint NOT NULL,
  `order_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `shipping_date` datetime NULL DEFAULT NULL,
  `delivery_date` date NULL DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` smallint NOT NULL,
  `updated_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` smallint NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `order_detail`;
CREATE TABLE `order_detail`  (
  `id` smallint NOT NULL AUTO_INCREMENT,
  `order_id` smallint NOT NULL,
  `product_id` smallint NOT NULL,
  `quantity` smallint NOT NULL,
  `price` decimal(10, 2) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` smallint NOT NULL,
  `updated_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` smallint NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uq_product_for_order`(`order_id`, `product_id`),
  CONSTRAINT `ck_price_product_greather_than_zero(0)` CHECK (price > 0)
);

DROP TABLE IF EXISTS `product`;
CREATE TABLE `product`  (
  `id` smallint NOT NULL AUTO_INCREMENT,
  `product_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `stock` smallint NOT NULL DEFAULT 0,
  `cost` decimal(10, 2) NOT NULL,
  `price` decimal(10, 2) NOT NULL,
  `barcode` int NOT NULL,
  `category_id` smallint NOT NULL,
  `provider_id` smallint NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` smallint NOT NULL,
  `updated_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` smallint NULL DEFAULT NULL,
  `deleted_at` datetime NULL DEFAULT NULL,
  `deleted_by` smallint NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `ck_price_product_greather_than_zero` CHECK (`price` > 0 ),
  CONSTRAINT `ck_price_product_greather_than_cost_product` CHECK (price > cost),
  UNIQUE INDEX `uq_no_repeating_barcode`(`barcode`)
);

DROP TABLE IF EXISTS `provider`;
CREATE TABLE `provider`  (
  `id` smallint NOT NULL AUTO_INCREMENT,
  `provider_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `contact_name` varchar(50) NOT NULL,
  `contact_email` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` smallint NOT NULL,
  `updated_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` smallint NULL DEFAULT NULL,
  `deleted_at` datetime NULL DEFAULT NULL,
  `deleted_by` smallint NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE `order` ADD CONSTRAINT `fk_order_provider` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`);
ALTER TABLE `order_detail` ADD CONSTRAINT `fk_order_detail_order` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`);
ALTER TABLE `order_detail` ADD CONSTRAINT `fk_order_detail_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`);
ALTER TABLE `product` ADD CONSTRAINT `fk_product_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);
ALTER TABLE `product` ADD CONSTRAINT `fk_product_provider` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`);
