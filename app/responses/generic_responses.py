"""Module for generic responses"""
import json

class Responses:
    """Class for CRUD responses"""
    @staticmethod
    def success_index(data):
        """Response for index request"""
        response = {
            'http_status_code': 200,
            'status': 'SUCCESS INDEX',
            'text': 'Success index',
            'data': data
        }
        return json.dumps(response, ensure_ascii=False), 200

    @staticmethod
    def success_show(data):
        """Response success for show request"""
        response = {
            'http_status_code': 200,
            'status': 'SUCCESS SHOW',
            'text': 'Success show of resource',
            'data': data
        }
        return json.dumps(response, ensure_ascii=False), 200

    @staticmethod
    def success_create(data):
        """Response success for create employee"""
        response = {
            'http_status_code': 201,
            'status': 'CREATED',
            'text': 'Resource created successfully',
            'data': data
        }
        return json.dumps(response, ensure_ascii=False), 201

    @staticmethod
    def success_update(data):
        """Response success for update employee"""
        response = {
            'http_status_code': 201,
            'status': 'UPDATED',
            'text': 'Resource updated successfully',
            'data': data
        }
        return json.dumps(response, ensure_ascii=False), 201

    @staticmethod
    def success_deleted(data):
        """ Response success for delete"""
        response = {
        'status_code': 201,
        'message': 'LOGICALLY DELETED SUCCESSFULLY',
        'data': data
        }
        return json.dumps(response, ensure_ascii=False), 201

    @staticmethod
    def success_shipping_date(data):
        """ Response success for add shipping date"""
        response = {
        'status_code': 201,
        'message': 'ADDED SHIPPING DATE SUCCESSFULLY',
        'data': data
        }
        return json.dumps(response, ensure_ascii=False), 201

    @staticmethod
    def success_destroy():
        """ Response for success destroy """
        response = {
        'status_code': 200,
        'message': 'DESTROYED SUCCESSFULLY',
        'data': None
        }
        return json.dumps(response, ensure_ascii=False), 200

    @staticmethod
    def not_found(data):
        """Bad response for show request"""
        response = {
            'http_status_code': 404,
            'status': 'NOT FOUND',
            'text': 'Resource was not foundd for the resource',
            'data': data
        }
        return json.dumps(response, ensure_ascii=False), 404

    @staticmethod
    def invalid_params():
        """Bad response for show request"""
        response = {
            'http_status_code': 500,
            'status': 'INVALID PARAMS',
            'text': 'The params sent are invalid',
            'data': 'The params sent are invalid'
        }
        return json.dumps(response, ensure_ascii=False), 500
