"""Main flask module"""
from flask import Flask
from db import db
from config import Config
from responses.generic_responses import Responses
from blueprints.category import category_bp
from blueprints.order import order_bp
from blueprints.product import product_bp
from blueprints.provider import provide_bp
from blueprints.order_detail import order_detail_bp
from errors.error_handler import InvalidParamsException

application = app = Flask(__name__)
app.config.from_object(Config())

app.register_blueprint(category_bp)
app.register_blueprint(order_bp)
app.register_blueprint(order_detail_bp)
app.register_blueprint(product_bp)
app.register_blueprint(provide_bp)

db.init_app(app)

@app.route('/')
def index():
    return 'Mi tiendita Oscar'

@app.errorhandler(InvalidParamsException)
def error_params(error):
  return Responses.invalid_params()


if __name__ == '__main__':
    application.run(host='0.0.0.0', debug=True)
