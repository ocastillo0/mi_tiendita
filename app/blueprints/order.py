""" Order Module """
import json
from sqlalchemy.exc import OperationalError, DataError, IntegrityError
from schemas.product_schema import ProductSchema
from errors.error_handler import InvalidParamsException
from flask import Blueprint, request
from datetime import datetime
from responses.generic_responses import Responses
from models.order_model import Order
from schemas.order_schema import OrderSchema

order_bp = Blueprint('order', __name__, url_prefix='/orders')

@order_bp.get('/')
def index():
  """ Get all orders """
  params = request.args
  order_list = Order().get_all(params)
  order = OrderSchema(many=True).dump(order_list)
  return Responses.success_index(order)

@order_bp.get('/not-shipped')
def show_not_shipped_orders():
  """ Get all not shipped orders """
  params = request.args
  order_list = Order().get_not_shipped_orders(params)
  order = OrderSchema(many=True).dump(order_list)
  return Responses.success_index(order)

@order_bp.get('/delivered')
def show_delivered_orders():
  """ Get all not shipped orders """
  params = request.args
  order_list = Order().get_delivered_orders(params)
  order = OrderSchema(many=True).dump(order_list)
  return Responses.success_index(order)

@order_bp.get('/shipped')
def show_shipped_orders():
  """ Get all shipped orders """
  params = request.args
  order_list = Order().get_shipped_orders(params)
  order = OrderSchema(many=True).dump(order_list)
  return Responses.success_index(order)

@order_bp.get('/<order_id>')
def show(order_id):
  """ Get a order by the given id """
  order = Order().find_by_params({'id': order_id})
  ord = OrderSchema(many=False).dump(order)
  if ord:
    return Responses.success_show(ord)
  return Responses.not_found(ord)

@order_bp.post('')
def create():
  """Create an order in DB"""
  params = json.loads(request.data)
  order = Order(**params)
  order.create()
  try:
      order = Order(**params)
      order.create()
  except OperationalError: 
      raise InvalidParamsException()
  except TypeError:
      raise InvalidParamsException()
  except DataError:
      raise InvalidParamsException()
  except IntegrityError:
      raise InvalidParamsException()
  created_ord = OrderSchema(many=False).dump(order.find_by_params(params))
  return Responses.success_create(OrderSchema(many=False).dump(created_ord))

@order_bp.patch('/<order_id>')
def patch(order_id):
    """Add the shipping date'"""
    add_sdate = Order().add_shipping_date(order_id)
    if add_sdate:
        return Responses.success_shipping_date(OrderSchema(many=False).dump(add_sdate))
    return Responses.not_found({})
    
@order_bp.put('/<order_id>')
def update(order_id):
    """Update the delivery date of order according to the given id"""
    body = json.loads(request.data)
    updated_ord = Order().update(order_id, body)
    if updated_ord:
        return Responses.success_update(
            OrderSchema(many=False).dump(updated_ord)
        )
    return Responses.not_found({})

@order_bp.delete("/<order_id>")
def destroy(order_id):
  """ Make a permanent delete of an order """
  response = Order().destroy(order_id)
  if response:
      return Responses.success_destroy()
  return Responses.not_found({"id": order_id})
