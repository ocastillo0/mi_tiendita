""" Product Module """
import json
from sqlalchemy.exc import OperationalError, DataError, IntegrityError
from schemas.product_schema import ProductSchema
from errors.error_handler import InvalidParamsException
from flask import Blueprint, request
from datetime import datetime
from responses.generic_responses import Responses
from models.product_model import Product
product_bp = Blueprint('product', __name__, url_prefix='/products')

@product_bp.get('/')
def index():
  """ Get all products """
  params = request.args
  product_list = Product().get_all(params)
  product = ProductSchema(many=True).dump(product_list)
  return Responses.success_index(product)

@product_bp.get('/<product_id>/sold-by-provider/')
def sold_by_provider(product_id):
  """Get sold products by provider"""
  product_list = Product().get_products_per_provider(product_id)
  product = ProductSchema(many=True).dump(product_list)
  return Responses.success_index(product)

@product_bp.get('<product_id>/provider/<provider_id>')
def price_product_provider(product_id, provider_id):
  """Get price by provider for a product"""
  product_list  = Product().get_product_price_per_provider(product_id,provider_id)
  product = ProductSchema(many=True).dump(product_list)
  return Responses.success_index(product)

@product_bp.post('')
def create():
  """Create a product in DB"""
  params = json.loads(request.data)
  product = Product(**params)
  product.create()
  created_prod = ProductSchema(many=False).dump(product.find_by_params(params))
  return Responses.success_create(ProductSchema(many=False).dump(created_prod))

@product_bp.get('/<product_id>')
def show(product_id):
  """ Get a product by the given id """
  product = Product().find_by_params({'id': product_id})
  prod = ProductSchema(many=False).dump(product)
  if prod:
    return Responses.success_show(prod)
  return Responses.not_found(prod)

@product_bp.patch('/<product_id>')
def patch(product_id):
    """Change the status of active'"""
    status = Product().logic_delete(product_id)
    if status:
        return Responses.success_deleted(ProductSchema(many=False).dump(status))
    return Responses.not_found({})

@product_bp.put('/<product_id>')
def update(product_id):
    """Update a product according to the given id"""
    body = json.loads(request.data)
    updated_prod = Product().update(product_id, body)
    if updated_prod:
        return Responses.success_update(
            ProductSchema(many=False).dump(updated_prod)
        )
    return Responses.not_found({})

@product_bp.delete("/<product_id>")
def destroy(product_id):
  """ Make a permanent delete of a product """
  response = Product().destroy(product_id)
  if response:
      return Responses.success_destroy()
  return Responses.not_found({"id": product_id})
