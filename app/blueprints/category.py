""" CATEGORY Module """
import json
from sqlalchemy.exc import OperationalError, DataError, IntegrityError
from schemas.product_schema import ProductSchema
from errors.error_handler import InvalidParamsException
from flask import Blueprint, request
from datetime import datetime
from responses.generic_responses import Responses
from models.category_model import Category
from schemas.category_schema import CategorySchema

category_bp = Blueprint('category', __name__, url_prefix='/categories')

@category_bp.get('/')
def index():
  """ Get all categories """
  params = request.args
  category_list = Category().get_all(params)
  category = CategorySchema(many=True).dump(category_list)
  return Responses.success_index(category)

@category_bp.get('/<category_id>')
def show(category_id):
  """ Get a category by the given id """
  category = Category().find_by_params({'id': category_id})
  cat = CategorySchema(many=False).dump(category)
  if cat:
    return Responses.success_show(cat)
  return Responses.not_found(cat)

@category_bp.post('')
def create():
  """Create a category in DB"""
  params = json.loads(request.data)
  category = Category(**params)
  category.create()  
  
  created_cat = CategorySchema(many=False).dump(category.find_by_params(params))
  return Responses.success_create(CategorySchema(many=False).dump(created_cat))

@category_bp.delete("/<category_id>")
def destroy(category_id):
  """ Make a permanent delete of a category """
  response = Category().destroy(category_id)
  if response:
      return Responses.success_destroy()
  return Responses.not_found({"id": category_id})
