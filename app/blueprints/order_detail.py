""" Order Detail Module """
import json
from sqlalchemy.exc import OperationalError, DataError, IntegrityError
#from schemas.product_schema import ProductSchema
from errors.error_handler import InvalidParamsException
from flask import Blueprint, request
from datetime import datetime
from responses.generic_responses import Responses
from models.order_detail_model import OrderDetail
from schemas.order_detail_schema import OrderDetailSchema

order_detail_bp = Blueprint('order_detail', __name__, url_prefix='/order-details')

@order_detail_bp.get('/')
def index():
  """ Get all details """
  params = request.args
  details_list = OrderDetail().get_all(params)
  detail = OrderDetailSchema(many=True).dump(details_list)
  return Responses.success_index(detail)

@order_detail_bp.post('')
def create():
  """Create a order detail in DB"""
  params = json.loads(request.data)
  detail = OrderDetail(**params)
  detail.create()  
  created_det = OrderDetailSchema(many=False).dump(detail.find_by_params(params))
  return Responses.success_create(OrderDetailSchema(many=False).dump(created_det))

@order_detail_bp.put('/<order_id>')
def update(order_id):
    """Update the detail of order according to the given id"""
    body = json.loads(request.data)
    updated_ord = OrderDetail().update(order_id, body)
    if updated_ord:
        return Responses.success_update(OrderDetailSchema(many=False).dump(updated_ord))
    return Responses.not_found({})

@order_detail_bp.delete("/<detail_id>")
def destroy(detail_id):
  """ Make a permanent delete of a category """
  response = OrderDetail().destroy(detail_id)
  if response:
      return Responses.success_destroy()
  return Responses.not_found({"id": detail_id})
