""" Provider Module """
import json
from schemas.provider_schema import ProviderSchema
from flask import Blueprint, request
from responses.generic_responses import Responses
from models.provider_model import Provider

provide_bp = Blueprint('provide', __name__, url_prefix='/providers')

@provide_bp.get('/')
def index():
  """ Get all providers """
  params = request.args
  provider_list = Provider().get_all(params)
  provider = ProviderSchema(many=True).dump(provider_list)
  return Responses.success_index(provider)


@provide_bp.post('')
def create():
  """Create a provider in DB"""
  params = json.loads(request.data)
  provider = Provider(**params)
  provider.create()
  created_pro = ProviderSchema(many=False).dump(provider.find_by_params(params))
  return Responses.success_create(ProviderSchema(many=False).dump(created_pro))

@provide_bp.get('/<provider_id>')
def show(provider_id):
  """ Get a provider by the given id """
  provider = Provider().find_by_params({'id': provider_id})
  pro = ProviderSchema(many=False).dump(provider)
  if pro:
    return Responses.success_show(pro)
  return Responses.not_found(pro)

@provide_bp.patch('/<provider_id>')
def patch(provider_id):
    """Change the status of active'"""
    status = Provider().logic_delete(provider_id)
    if status:
        return Responses.success_deleted(ProviderSchema(many=False).dump(status))
    return Responses.not_found({})

@provide_bp.put('/<provider_id>')
def update(provider_id):
    """Update a provider according to the given id"""
    body = json.loads(request.data)
    updated_pro = Provider().update(provider_id, body)
    if updated_pro:
        return Responses.success_update(
            ProviderSchema(many=False).dump(updated_pro)
        )
    return Responses.not_found({})

@provide_bp.delete("/<provider_id>")
def destroy(provider_id):
  """ Make a permanent delete of a provider """
  response = Provider().destroy(provider_id)
  if response:
      return Responses.success_destroy()
  return Responses.not_found({"id": provider_id})
