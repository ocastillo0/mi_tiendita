"""SQL Alchemy DB module"""
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()