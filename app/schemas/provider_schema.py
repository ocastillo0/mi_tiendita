"""Schema for Provider"""
from ma import ma

class ProviderSchema(ma.Schema):
    """Provider Schema class"""
    class Meta:
        """Meta Class for Provider Schema"""
        fields = (
            "id",
            "provider_name",
            "contact_name",
            "contact_email",
            "active"
        )