"""Schema for Category"""
from ma import ma

class CategorySchema(ma.Schema):
    """Category Schema class"""
    class Meta:
        """Meta Class for Category Schema"""
        fields = (
            "id",
            "name"
        )