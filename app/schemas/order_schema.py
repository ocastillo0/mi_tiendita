"""Schema for Order"""
from ma import ma

class OrderSchema(ma.Schema):
    """Order Schema class"""
    class Meta:
        """Meta Class for Order Schema"""
        fields = (
            "id",
            "provider_id",
            "order_date",
            "shipping_date",
            "delivery_date"
        )