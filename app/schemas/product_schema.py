"""Schema for Product"""
from ma import ma

class ProductSchema(ma.Schema):
    """Product Schema class"""
    class Meta:
        """Meta Class for Product Schema"""
        fields = (
            "id",
            "product_name",
            "stock",
            "cost",
            "price",
            "barcode",
            "provider_id",
            "category_id",
            "active"
        )