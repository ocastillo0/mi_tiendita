"""Schema for Order Detail"""
from ma import ma

class OrderDetailSchema(ma.Schema):
    """Order Detail Schema class"""
    class Meta:
        """Meta Class for Order Detail Schema"""
        fields = (
            "id",
            "order_id",
            "product_id",
            "quantity",
            "price"
        )