"""Model for category"""
from datetime import datetime
from db import db
from schemas.category_schema import CategorySchema
import random
from errors.error_handler import InvalidParamsException

class Category(db.Model):
    """Category Model class"""
    id = db.Column(db.SmallInteger, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    created_at = db.Column(db.DateTime, nullable=False)
    created_by = db.Column(db.SmallInteger, nullable=False)  
    
    fields = (
            "id",
            "name",
            "create_at",
            "create_by"
        )

    def __validate_params(self,params):
        """Validate if sent params are valid"""
        for param in params:
            if param not in self.fields:
                raise InvalidParamsException()
    

    def get_all(self, params=None):
        """Get all categories"""
        self.__validate_params(params)
        return self.query.filter_by(**params).all()

    def find_by_params(self, params):
        """Get the first resource by the given params"""
        return self.query.filter_by(**params).first()

    def create(self):
        """Create a new category in DB"""
        self.created_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.created_by = random.randint(1, 100)
        db.session.add(self)
        db.session.commit()

    def destroy(self, cat_id):
        """Destroy a category in  DB"""
        category = self.find_by_params({"id": cat_id})
        if category:
            db.session.delete(category)
            db.session.commit()
            return True
        return False
