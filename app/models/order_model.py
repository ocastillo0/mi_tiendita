"""Model for Order"""
from datetime import datetime
from db import db
from schemas.order_schema import OrderSchema
import random 
import sqlalchemy
from errors.error_handler import InvalidParamsException
from models.provider_model import Provider

class Order(db.Model):
    """Order Model class"""
    id = db.Column(db.SmallInteger, primary_key=True)
    provider_id = db.Column(db.SmallInteger,db.ForeignKey(Provider.id),nullable=False)
    order_date = db.Column(db.DateTime, nullable=False)
    shipping_date = db.Column(db.DateTime, nullable=True)
    delivery_date = db.Column(db.Date, nullable=True)
    created_at = db.Column(db.DateTime, nullable=False)
    created_by = db.Column(db.SmallInteger, nullable=False)
    updated_at = db.Column(db.DateTime, nullable=True)
    updated_by = db.Column(db.SmallInteger, nullable=True)   
    
    fields = (
            "id",
            "provider_id",
            "order_date",
            "shipping_date",
            "delivery_date",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by"
        )

    def __validate_params(self,params):
        """Validate if sent params are valid"""
        for param in params:
            if param not in self.fields:
                raise InvalidParamsException()

    def get_all(self, params=None):
        """Get all orders"""
        self.__validate_params(params)
        return self.query.filter_by(**params).all()

    def get_not_shipped_orders(self, params=None):
        """Get all shipped orders"""
        self.__validate_params(params)
        return self.query.filter_by(shipping_date = sqlalchemy.sql.null(), **params).all()

    def get_shipped_orders(self, params=None):
        """Get all not shipped orders"""
        self.__validate_params(params)
        return self.query.filter(Order.shipping_date != sqlalchemy.sql.null()).all()
    
    def get_delivered_orders(self, params=None):
        """Get all delivered orders"""
        self.__validate_params(params)
        return self.query.filter(Order.delivery_date < datetime.now()).all()

    def find_by_params(self, params):
        """Get the first resource by the given params"""
        return self.query.filter_by(**params).first()

    def create(self):
        """Create a new order in DB"""
        self.order_date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.created_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.created_by = random.randint(1, 100)
        db.session.add(self)
        db.session.commit()

    def add_shipping_date(self, ord_id):
        """Add the shipping date for order"""
        order = self.find_by_params({"id": ord_id, "shipping_date": sqlalchemy.sql.null()})
        if order:
            order.shipping_date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            order.updated_by = random.randint(1, 100)
            db.session.commit()
            return order
        return None

    def update(self, ord_id, params):
        """Update the delivery date of order in DB"""
        order = self.find_by_params({"id": ord_id, "delivery_date": sqlalchemy.sql.null()})
        if order:
            for param, value in params.items():
                setattr(order, param, value)
            db.session.commit()
            return self.find_by_params({"id": ord_id})
        return None

    def destroy(self, ord_id):
        """Destroy an order in  DB"""
        order = self.find_by_params({"id": ord_id})
        if order:
            db.session.delete(order)
            db.session.commit()
            return True
        return False