"""Model for Product"""
from datetime import datetime
from db import db
from schemas.product_schema import ProductSchema
import random 
import sqlalchemy
from errors.error_handler import InvalidParamsException
from models.category_model import Category
from models.provider_model import Provider

class Product(db.Model):
    """Product Model class"""
    id = db.Column(db.SmallInteger, primary_key=True)
    product_name = db.Column(db.String(50), nullable=False)
    stock = db.Column(db.SmallInteger, nullable=False)
    cost = db.Column(db.Float,nullable=False)
    price = db.Column(db.Float,nullable=False)
    barcode = db.Column(db.Integer, nullable=False)
    provider_id = db.Column(db.SmallInteger, db.ForeignKey(Provider.id), nullable=True)
    category_id = db.Column(db.SmallInteger, db.ForeignKey(Category.id), nullable=True)
    active = db.Column(db.SmallInteger, nullable = False, default = 1)
    created_at = db.Column(db.DateTime, nullable=False)
    created_by = db.Column(db.SmallInteger, nullable=False)
    updated_at = db.Column(db.DateTime, nullable=True)
    updated_by = db.Column(db.SmallInteger, nullable=True)   
    deleted_at = db.Column(db.DateTime, nullable=True)
    deleted_by = db.Column(db.SmallInteger, nullable=True)   
    
    fields = (
            "id",
            "product_name",
            "stock",
            "cost",
            "price",
            "barcode",
            "provider_id",
            "category_id",
            "active",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by",
            "deleted_at",
            "deleted_by"
        )

    def __validate_params(self,params):
        """Validate if sent params are valid"""
        for param in params:
            if param not in self.fields:
                raise InvalidParamsException()

    def get_all(self, params=None):
        """Get all products"""
        self.__validate_params(params)
        return self.query.filter_by(active= 1,**params).all()
    
    def get_products_per_provider(self, prov_id):
        """Get sold products by provider"""
        return(db.session.query(Product.product_name, Provider.provider_name)
                                .join(Provider, Product.provider_id == Provider.id)
                                .filter(Provider.id == prov_id))

    def get_product_price_per_provider(self, prod_id, prov_id):
        """Get price by provider for a product"""
        return(db.session.query(Product.price, Provider.provider_name)
                                .join(Provider, Product.provider_id == Provider.id)
                                .filter(Product.id == prod_id)
                                .filter(Provider.id == prov_id))

    def find_by_params(self, params):
        """Get the first resource by the given params"""
        return self.query.filter_by(**params).first()

    def create(self):
        """Create a new product in DB"""
        self.created_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.created_by = random.randint(1, 100)
        db.session.add(self)
        db.session.commit()
    
    def destroy(self, prod_id):
        """Destroy an product in DB"""
        order = self.find_by_params({"id": prod_id})
        if order:
            db.session.delete(order)
            db.session.commit()
            return True
        return False

    def update(self, prod_id, params):
        """Update a product in DB"""
        product = self.find_by_params({"id": prod_id, "active": 1})
        if product:
            for param, value in params.items():
                setattr(product, param, value)
            db.session.commit()
            return self.find_by_params({"id": prod_id})
        return None
    
    def logic_delete(self, pro_id):
        """Logic delete for a product"""
        product = self.find_by_params({"id": pro_id, "deleted_at": sqlalchemy.sql.null()})
        if product:
            product.deleted_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            product.deleted_by = random.randint(1, 100)
            product.active = 0
            db.session.commit()
            return product
        return None


