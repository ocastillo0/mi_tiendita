"""Model for Provider"""
from datetime import datetime
from db import db
from schemas.provider_schema import ProviderSchema
import random 
import sqlalchemy
from errors.error_handler import InvalidParamsException

class Provider(db.Model):
    """Provider Model class"""
    id = db.Column(db.SmallInteger, primary_key=True)
    provider_name = db.Column(db.String(50), nullable=False)
    contact_name = db.Column(db.String(50), nullable=False)
    contact_email = db.Column(db.String(100), nullable=False)
    active = db.Column(db.SmallInteger, nullable=False, default=1)
    created_at = db.Column(db.DateTime, nullable=False)
    created_by = db.Column(db.SmallInteger, nullable=False)
    updated_at = db.Column(db.DateTime, nullable=True)
    updated_by = db.Column(db.SmallInteger, nullable=True)   
    deleted_at = db.Column(db.DateTime, nullable=True)
    deleted_by = db.Column(db.SmallInteger, nullable=True)   
    
    fields = (
            "id",
            "provider_name",
            "contact_name",
            "contact_email",
            "active",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by",
            "deleted_at",
            "deleted_by"
        )

    def __validate_params(self,params):
        """Validate if sent params are valid"""
        for param in params:
            if param not in self.fields:
                raise InvalidParamsException()

    
    def get_all(self, params=None):
        """Get all products"""
        self.__validate_params(params)
        return self.query.filter_by(active= 1,**params).all()
    
    def find_by_params(self, params):
        """Get the first resource by the given params"""
        return self.query.filter_by(**params).first()
    
    def create(self):
        """Create a new provider in DB"""
        self.created_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.created_by = random.randint(1, 100)
        db.session.add(self)
        db.session.commit()
    
    def destroy(self, pro_id):
        """Destroy an provider in DB"""
        order = self.find_by_params({"id": pro_id})
        if order:
            db.session.delete(order)
            db.session.commit()
            return True
        return False

    def update(self, pro_id, params):
        """Update a provider in DB"""
        provider = self.find_by_params({"id": pro_id, "active": 1})
        if provider:
            for param, value in params.items():
                setattr(provider, param, value)
            db.session.commit()
            return self.find_by_params({"id": pro_id})
        return None
    
    def logic_delete(self, pro_id):
        """Logic delete for a provider"""
        provider = self.find_by_params({"id": pro_id, "deleted_at": sqlalchemy.sql.null()})
        if provider:
            provider.deleted_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            provider.deleted_by = random.randint(1, 100)
            provider.active = 0
            db.session.commit()
            return provider
        return None