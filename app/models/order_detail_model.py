"""Model for order detail"""
from datetime import datetime
from db import db
from schemas.order_detail_schema import OrderDetailSchema
import random
from errors.error_handler import InvalidParamsException
from models.product_model import Product
from models.order_model import Order

class OrderDetail(db.Model):
    """Order detail Model class"""
    id = db.Column(db.SmallInteger, primary_key=True)
    order_id = db.Column(db.SmallInteger, db.ForeignKey(Order.id),nullable=False)
    product_id = db.Column(db.SmallInteger, db.ForeignKey(Product.id), nullable=False)
    quantity = db.Column(db.SmallInteger, nullable=False)
    price= db.Column(db.Float, nullable=False)
    created_at = db.Column(db.DateTime, nullable=False)
    created_by = db.Column(db.SmallInteger, nullable=False)  
    updated_at = db.Column(db.DateTime, nullable=False)
    updated_by = db.Column(db.SmallInteger, nullable=False) 
    
    fields = (
            "id",
            "order_id",
            "product_id",
            "quantity",
            "price",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by"
        )

    def __validate_params(self,params):
        """Validate if sent params are valid"""
        for param in params:
            if param not in self.fields:
                raise InvalidParamsException()
    
    def get_all(self, params=None):
        """Get all categories"""
        self.__validate_params(params)
        return self.query.filter_by(**params).all()
    
    def find_by_params(self, params):
        """Get the first resource by the given params"""
        return self.query.filter_by(**params).first()
    
    def create(self):
        """Create a new category in DB"""
        self.created_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.created_by = random.randint(1, 100)
        db.session.add(self)
        db.session.commit()
    
    def update(self, ord_id, params):
        """Update the delivery date of order in DB"""
        order = self.find_by_params({"id": ord_id})
        self.updated_by = random.randint(1, 100)
        if order:
            for param, value in params.items():
                setattr(order, param, value)
            db.session.commit()
            return self.find_by_params({"id": ord_id})
        return None
    
    def destroy(self, det_id):
        """Destroy a detail in DB"""
        detail = self.find_by_params({"id": det_id})
        if detail:
            db.session.delete(detail)
            db.session.commit()
            return True
        return False
